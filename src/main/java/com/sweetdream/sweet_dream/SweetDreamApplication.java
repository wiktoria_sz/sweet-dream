package com.sweetdream.sweet_dream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SweetDreamApplication {

    public static void main(String[] args) {
        SpringApplication.run(SweetDreamApplication.class, args);
    }
}
