package com.sweetdream.sweet_dream.dao;

import com.sweetdream.sweet_dream.model.Cake;
import com.sweetdream.sweet_dream.model.Recipe;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CakeDao extends CrudRepository<Cake, Long> {
    public Cake getCakeByName(String name);
    public List<Cake> findAllByType(String type);

}
