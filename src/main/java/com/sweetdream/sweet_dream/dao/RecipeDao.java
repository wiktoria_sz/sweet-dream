package com.sweetdream.sweet_dream.dao;

import com.sweetdream.sweet_dream.model.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeDao extends CrudRepository<Recipe, Long> {
}
