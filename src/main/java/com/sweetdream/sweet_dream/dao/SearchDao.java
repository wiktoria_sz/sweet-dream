package com.sweetdream.sweet_dream.dao;

import com.sweetdream.sweet_dream.model.Cake;
import com.sweetdream.sweet_dream.model.Recipe;
import com.sweetdream.sweet_dream.model.Search;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SearchDao {
    public List<Cake> findCakeByType(String name, String type);

    public List<Recipe> findRecipesByAuthor(String author);
}