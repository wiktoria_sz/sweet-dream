package com.sweetdream.sweet_dream.controllers;

import com.sweetdream.sweet_dream.dao.CakeDao;
import com.sweetdream.sweet_dream.dao.RecipeDao;
import com.sweetdream.sweet_dream.dao.SearchDao;
import com.sweetdream.sweet_dream.model.Cake;
import com.sweetdream.sweet_dream.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AddController {
    @Autowired
    private CakeDao cakeDao;
    @Autowired
    private RecipeDao recipeDao;
    @Autowired
    private SearchDao searchDao;


    @GetMapping(path = "cakes/add")
    public String addNewCake() {
        return "cakes/add";
    }


    @PostMapping("/cakes")
    // this path adds a cake and redirects to the main search page
    // Attention - url can't be very long
    // todo: search for a solution to small data input
    public String createCake(@ModelAttribute Cake cake,
                             RedirectAttributes redirectAttributes) {
        // @RequestParam means it is a parameter from the GET or POST request
        cakeDao.save(cake);
        redirectAttributes.addFlashAttribute("message", "Cake was added successfully.");
        return "redirect:/";
    }


    @PostMapping("cakes/addRecipe")
    // adds a recipe to a cake
    public String addRecipe(@ModelAttribute Recipe recipe,
                            RedirectAttributes redirectAtt) {
        recipeDao.save(recipe);
        redirectAtt.addFlashAttribute("message2", "Recipe was added successfully.");
        return "redirect:/cakes/" + recipe.getCake().getId();
    }


}
