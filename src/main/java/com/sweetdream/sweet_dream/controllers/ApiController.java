package com.sweetdream.sweet_dream.controllers;


import com.sweetdream.sweet_dream.dao.CakeDao;
import com.sweetdream.sweet_dream.model.Cake;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ApiController {

    @Autowired
    private CakeDao cakeDao;



    @GetMapping("/cakes/info")
    public String goToInfo() {
        return "cakes/info";
    }


    @GetMapping(path = "cakes/all")
    public @ResponseBody
    Iterable<Cake> getAllCakes() {
        return cakeDao.findAll();
    }



}
