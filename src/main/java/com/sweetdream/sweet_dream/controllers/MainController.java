package com.sweetdream.sweet_dream.controllers;

import com.sweetdream.sweet_dream.dao.CakeDao;
import com.sweetdream.sweet_dream.dao.RecipeDao;
import com.sweetdream.sweet_dream.dao.SearchDao;
import com.sweetdream.sweet_dream.model.Cake;
import com.sweetdream.sweet_dream.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MainController {
    @Autowired
    private CakeDao cakeDao;
    @Autowired
    private RecipeDao recipeDao;
    @Autowired
    private SearchDao searchDao;


    @GetMapping("/")
    public String findCake(ModelMap map) {
        map.addAttribute("cake", cakeDao);
        return "cakes/index";
    }


    @GetMapping("cakes/search_author")
    public String show(@RequestParam String author, ModelMap map) {
        if (!searchDao.findRecipesByAuthor(author).isEmpty()) {
            map.put("recipes", searchDao.findRecipesByAuthor(author));
            return "cakes/author_rec";
        }
        return "cakes/noFound";
    }


    @PostMapping("/cakes/search")
    public String searchCake(@RequestParam(required = false) String name,
                             @RequestParam(required = false) String type) {

        if (!searchDao.findCakeByType(name, type).isEmpty()) {
            return "redirect:/cakes/" + (searchDao.findCakeByType(name, type).get(0)).getId();
        }
        return "cakes/noFound";
    }


    @GetMapping("cakes/{id}")
    public String show(@PathVariable Long id, ModelMap map) {
        map.put("cake", cakeDao.findById(id).get());
        return "cakes/show";
    }


}