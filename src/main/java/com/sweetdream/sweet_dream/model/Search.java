package com.sweetdream.sweet_dream.model;

import com.sweetdream.sweet_dream.dao.CakeDao;
import com.sweetdream.sweet_dream.dao.RecipeDao;
import com.sweetdream.sweet_dream.dao.SearchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Search implements SearchDao {


    @Autowired
    private CakeDao cakeDao;
    @Autowired
    private RecipeDao recipeDao;

    @Override
    public List<Cake> findCakeByType(String name, String type) {

        List<Cake> cakesByTypeList = cakeDao.findAllByType(type);
        List<Cake> shouldBeOneCake = new ArrayList<>();

        for (Cake cake : cakesByTypeList) {
            if ((cakeDao.getCakeByName(name)).getName().toLowerCase().trim()
                    .equals(cake.getName().toLowerCase().trim())) {
                shouldBeOneCake.add(cake);
                return shouldBeOneCake;
            }
        }
        return shouldBeOneCake;
    }


    @Override
    public List<Recipe> findRecipesByAuthor(String author) {

        List<Recipe> recipesByAuthorList = new ArrayList<>();

        for(Recipe recipe :recipeDao.findAll()){
                if (recipe.getAuthor().trim().toLowerCase().contains(author.trim())
                || author.trim().toLowerCase().contains(recipe.getAuthor().trim().toLowerCase())) {
                    recipesByAuthorList.add(recipe);
                }
            }
        return recipesByAuthorList;

    }
}
